import React, { Component } from "react";
import "./index.css";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
    inputText: ""
  };

  submitBox = (event) => {
    if(event.key === "Enter"){
      let arr = this.state.todos;
      arr.push({id: 6, userId: 2, title: this.state.inputText, completed: false});
      this.setState({todos: arr, inputText: ""});
      document.getElementsByClassName("new-todo")[0].value = "";
    }
    else{
      this.setState({todos: this.state.todos, inputText: event.target.value});
    }
  }

  toggleCompletion = (e) => {
    let arr = this.state.todos.slice(0, e.target.getAttribute("identifier"));
    let arr2 = this.state.todos.slice(e.target.getAttribute("identifier"), this.state.todos.length);
    arr2.shift();
    let temp = this.state.todos[e.target.getAttribute("identifier")];
    this.setState({todos: arr.concat([{id: temp.id, userId: temp.userId, title: temp.title, completed: !temp.completed}], arr2), inputText: this.state.inputText});
  }

  delete = (e) => {
    let arr = this.state.todos;
    console.log(e.target.parentElement.parentElement.getAttribute("Identifier"));
    arr.splice(e.target.parentElement.parentElement.getAttribute("Identifier"), 1);
    console.log(arr);
    this.setState({todos: arr,inputText: this.state.inputText})
  }
  clear = () => {
    let arr =[];
    for (let i = 0; i < this.state.todos.length; i ++){
      if(this.state.todos[i].completed !== true)
        arr.push(this.state.todos[i]);
    }
    this.setState({todos: arr, inputText: this.state.inputText});
  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
            onKeyUp={this.submitBox}
          />
        </header>
        <TodoList todos={this.state.todos} action = {this.toggleCompletion} delete = {this.delete}/>
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick = {this.clear}>Clear completed</button>
        </footer>
      </section>
    );
  }
}
class TodoItem extends Component {
  render() {
    return (
      <li identifier = {this.props.identifier} className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onClick = {this.props.action}
            identifier = {this.props.identifier}
            readOnly
          />
          <label>{this.props.title}</label>
          <button className="destroy" onClick = {this.props.delete}/>
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo,index) => (
            <TodoItem title={todo.title} completed={todo.completed} key={index} identifier = {index} action = {this.props.action} delete = {this.props.delete}/>
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
